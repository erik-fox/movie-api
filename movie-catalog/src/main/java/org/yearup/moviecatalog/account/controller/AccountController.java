package org.yearup.moviecatalog.account.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.yearup.moviecatalog.security.model.Account;
import org.yearup.moviecatalog.account.service.AccountServiceImpl;
import org.yearup.moviecatalog.movie.dto.SuccessfulResponseDetail;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AccountController {

    @Autowired
    private AccountServiceImpl accountServiceImpl;

    @RequestMapping(method = RequestMethod.GET, value = "/accounts")
    public ResponseEntity<List<Account>> getAllAccounts(){
        List<Account> accounts = accountServiceImpl.getAllAccounts();
        SuccessfulResponseDetail successfulResponseDetail = new SuccessfulResponseDetail(HttpStatus.OK.value(), "Success", accounts);
        return new ResponseEntity(successfulResponseDetail, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{userId}")
    public ResponseEntity<?> getAccountById(@PathVariable Long userId){
        Optional<Account> accounts = accountServiceImpl.getAccountById(userId);
        SuccessfulResponseDetail successfulResponseDetail = new SuccessfulResponseDetail(HttpStatus.OK.value(), "Success", accounts);
        return new ResponseEntity(successfulResponseDetail, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/accounts")
    public ResponseEntity<?> createAccount(@Valid @RequestBody Account account){
        Account a = accountServiceImpl.createAccount(account);
        HttpHeaders headers = new HttpHeaders();
        URI newAccountUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/accounts")
                .buildAndExpand(account.getUserId())
                .toUri();
        headers.setLocation(newAccountUri);
        SuccessfulResponseDetail successfulResponseDetail = new SuccessfulResponseDetail(HttpStatus.CREATED.value(), "Created Account", account);
        return new ResponseEntity<>(successfulResponseDetail, headers, HttpStatus.CREATED);
    }
    @RequestMapping(method = RequestMethod.PUT, value = "/accounts/{userId}")
    public ResponseEntity<?> updateAccount(@RequestBody Account account, @PathVariable Long userId){
        accountServiceImpl.updateAccount(account, userId);
        SuccessfulResponseDetail successfulResponseDetail = new SuccessfulResponseDetail(HttpStatus.ACCEPTED.value(), "Updated Account", account);
        return new ResponseEntity<>(successfulResponseDetail, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/accounts/{userId}")
    public ResponseEntity<?> deleteAccount(@PathVariable Long userId){
        accountServiceImpl.deleteAccountById(userId);
        SuccessfulResponseDetail successfulResponseDetail = new SuccessfulResponseDetail(HttpStatus.NO_CONTENT.value(), "Deleted Account", null);
        return new ResponseEntity<>(successfulResponseDetail, HttpStatus.NO_CONTENT);
    }
}
