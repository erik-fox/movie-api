//package org.yearup.moviecatalog.account.dto;
//
//import java.util.UUID;
//
//public class AccountsDTO {
//
//    public UUID userId;
//
//    public String userName;
//
//    public String password;
//
//    public String email;
//
//    public AccountsDTO(UUID userId, String userName, String password, String email) {
//        this.userId = userId;
//        this.userName = userName;
//        this.password = password;
//        this.email = email;
//    }
//
//    public UUID getUserId() {
//        return userId;
//    }
//
//    public void setUserId(UUID userId) {
//        this.userId = userId;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    @Override
//    public String toString() {
//        return "AccountsDTO{" +
//                "userId=" + userId +
//                ", userName='" + userName + '\'' +
//                ", password='" + password + '\'' +
//                ", email='" + email + '\'' +
//                '}';
//    }
//}
