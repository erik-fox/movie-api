package org.yearup.moviecatalog.account.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yearup.moviecatalog.security.model.Account;
import org.yearup.moviecatalog.security.reopository.AccountRepository;
import org.yearup.moviecatalog.movie.exception.ResourceNotFoundException;
import org.yearup.moviecatalog.security.reopository.MovieRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl {

    @Autowired
    private AccountRepository accountRepository;
    private MovieRepository movieRepository;

//    @Override
//    public AccountsDTO getAccount(UUID userId) {
//        if (accountRepository.findById(userId).isPresent()) {
//            Accounts fetchedAccount = accountRepository.findById(userId).get();
//            return new AccountsDTO(fetchedAccount.getUserId(),
//                    fetchedAccount.getUserName(),
//                    fetchedAccount.getPassword(),
//                    fetchedAccount.getEmail());
//        } else {
//            return null;
//        }
//    }

    protected void verifyAccount(Long userId) throws ResourceNotFoundException {
        Optional<Account> accounts = accountRepository.findById(userId);
        if (accounts.isEmpty()){
            throw new ResourceNotFoundException("User with id " + userId + " not found");
        }
    }

    public Account createAccount(Account account){
       return accountRepository.save(account);
    }



    public List<Account> getAllAccounts(){
        List<Account> accounts = new ArrayList<>();
        accountRepository.findAll().forEach(accounts :: add);
        return accounts;
    }

    public Optional<Account> getAccountById(Long userId){
        return accountRepository.findById(userId);
    }

    public void updateAccount(Account account, Long userId){
        for (Account account1 : accountRepository.findAll()){
            if (account1.getUserId().equals(userId)){
                accountRepository.save(account);
            }
        }
    }

    public void deleteAccountById(Long userId){
        accountRepository.deleteById(userId);
    }

}

