package org.yearup.moviecatalog.security.reopository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.yearup.moviecatalog.security.model.Movie;

@Repository
@CrossOrigin(origins = "http://localhost:4200")
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
