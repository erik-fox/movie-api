package org.yearup.moviecatalog.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.yearup.moviecatalog.security.reopository.AccountRepository;
import org.yearup.moviecatalog.security.model.Movie;
import org.yearup.moviecatalog.security.dto.MovieDTO;
import org.yearup.moviecatalog.movie.exception.ResourceNotFoundException;
import org.yearup.moviecatalog.security.reopository.MovieRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AuthService authService;

    public List<Movie> getAllMovies() {
        List<Movie> movieList = new ArrayList<>();
        movieRepository.findAll().forEach(movieList::add);
        return movieList;
    }

    public Optional<Movie> getMovieById(Long id) {
        verifyMovie(id);
        return movieRepository.findById(id);
    }

    public void createMovie(MovieDTO movieDTO) {
        Movie movie = new Movie();
        movie.setTitle(movieDTO.getTitle());
        movie.setDescription(movieDTO.getDescription());
        User username = authService.getCurrentUser().orElseThrow(() ->
                new IllegalArgumentException("No Account logged in"));
        movie.setUsername(username.getUsername());
        movieRepository.save(movie);
    }

    //Needs Work
//
//    public void addMovieToAccount(Movie movie, Long userId){
//        List<Movie> movies = new ArrayList<>();
//        for (Accounts account : accountRepository.findAll()){
//            if (account.getUserId().equals(userId)){
//
//                movies = account.getMovies();
//                movies.add(movie);
//                account.setMovies(movies);
//                movieRepository.saveAll(movies);
//
//               movie.setAccounts(account);
//             movieRepository.save(movie);
//            }
//        }
//    }

    public void updateMovie(Movie movie, Long id) {
        verifyMovie(id);
        for (Movie movie1: movieRepository.findAll()) {
            if(movie1.getId().equals(id)) {
                movieRepository.save(movie);
            }
        }
    }

    public void deleteMovieById(Long id) {
        verifyMovie(id);
        movieRepository.deleteById(id);
    }

    public void verifyMovie(Long id) throws ResourceNotFoundException {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isEmpty()) {
            throw new ResourceNotFoundException("Movie with id " + id + " not found");

        }
    }
}
